import Vue from 'vue'
import Router from 'vue-router'
import Contact from '@/pages/Contact'
import HomePage from '@/pages/HomePage'
import CV from '@/pages/CV'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/cv',
      name: 'cv',
      component: CV
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    }
  ]
})
